<?php
// This file is part of TeacherBoard
//
// TeacherBoard is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TeacherBoard is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
//
// @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>

global $CFG;

include_once($CFG->dirroot . '/course/lib.php');
include_once($CFG->libdir . '/badgeslib.php');
include_once($CFG->dirroot . '/course/modlib.php');
require_once($CFG->dirroot . '/enrol/manual/locallib.php');
require_once($CFG->libdir . '/formslib.php');

class view_test extends advanced_testcase
{
	/*
	 * emulate view
	 *
	 */

	function test1()
		{
		global $USER, $DB, $CFG, $OUTPUT;
		$this->resetAfterTest(true);
		$PAGE = new moodle_page();
		$PAGE->set_context(context_system::instance());
		$this->setAdminUser();
		$USER->editing = 1;

		// create activity
		$course = $this->getDataGenerator()->create_course(
						array('fullname'=>'test millionnaire')
					);
					
		
		$name = 'My Millionnaire';
        $params = array('course' => $course->id, 'name' => $name);
        $millionnaire = $this->getDataGenerator()->create_module('millionnaire', $params);					
					

		// emulate GET request
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = 'view.php?id=' . $millionnaire->cmid;
		$_GET['id'] = $millionnaire->cmid;

		ob_start();
		include($CFG->dirroot . '/mod/millionnaire/view.php');
		$a=ob_get_contents();
		ob_end_clean();

		$this->AssertContains("xhReq.open(\"GET\", \"/mod/millionnaire/validate.ajax.php?mid=".$millionnaire->id."&sesskey=".$USER->sesskey."\", false);", $a);
		$this->AssertContains("<h2>".$name."</h2>", $a);
	}


	/*
	 * emulate view - no module id
	 *
	 */

	function test2()
		{
		global $USER, $DB, $CFG, $OUTPUT;
		$this->resetAfterTest(true);
		$PAGE = new moodle_page();
		$PAGE->set_context(context_system::instance());
		$this->setAdminUser();
		$USER->editing = 1;

		// create activity
		$course = $this->getDataGenerator()->create_course(
						array('fullname'=>'test millionnaire')
					);
					
		
		$name = 'My Millionnaire';
        $params = array('course' => $course->id, 'name' => $name);
        $millionnaire = $this->getDataGenerator()->create_module('millionnaire', $params);					
					

		// emulate GET request
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = 'view.php?id=' . $millionnaire->cmid;

		//$_GET['id'] = $millionnaire->cmid;

		try {
			include($CFG->dirroot . '/mod/millionnaire/view.php');
		}
		catch(Exception $e) {
			$this->AssertContains("error/You must specify a course_module ID or an instance ID", $e->getMessage());
		}

	}
	/*
	 * emulate view
	 *
	 */

	function test3()
		{
		global $USER, $DB, $CFG, $OUTPUT;
		$this->resetAfterTest(true);
		$PAGE = new moodle_page();
		$PAGE->set_context(context_system::instance());
		$this->setAdminUser();
		$USER->editing = 1;

		// create activity
		$course = $this->getDataGenerator()->create_course(
						array('fullname'=>'test millionnaire')
					);
					
		
		$name = 'My Millionnaire';
        $params = array('course' => $course->id, 'name' => $name);
        $millionnaire = $this->getDataGenerator()->create_module('millionnaire', $params);					
					

		// emulate GET request
		$_SERVER['REQUEST_METHOD'] = 'GET';
		$_SERVER['REQUEST_URI'] = 'validate.ajax.php?mid=' . $millionnaire->id . '&sesskey=' . $USER->sesskey;
		$_GET['mid'] = $millionnaire->id;
		$_GET['sesskey'] = $USER->sesskey;

		ob_start();
		include($CFG->dirroot . '/mod/millionnaire/validate.ajax.php');
		$a=ob_get_contents();
		ob_end_clean();

		$this->AssertContains("{\"status\":\"OK\",\"contents\":\"C'est mon dernier mot Jean-Pierre\"}", $a);

	}

}
