<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants for module millionnaire
 *
 * All the core Moodle functions, neeeded to allow the module to work
 * integrated in Moodle should be placed here.
 * All the millionnaire specific functions, needed to implement all the module
 * logic, should go to locallib.php. This will help to save some memory when
 * Moodle is performing actions across all modules.
 *
 * @package    mod_millionnaire
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/** example constant */
//define('NEWMODULE_ULTIMATE_ANSWER', 42);


define('MOD_MILLIONNAIRE_FRANKY','mod_millionnaire');
define('MOD_MILLIONNAIRE_LANG','mod_millionnaire');
define('MOD_MILLIONNAIRE_TABLE','millionnaire');
define('MOD_MILLIONNAIRE_USERTABLE','millionnaire');
define('MOD_MILLIONNAIRE_MODNAME','millionnaire');
define('MOD_MILLIONNAIRE_URL','/mod/millionnaire');
define('MOD_MILLIONNAIRE_CLASS','mod_millionnaire');
define('MOD_MILLIONNAIRE_GRADEHIGHEST', 0);
define('MOD_MILLIONNAIRE_GRADELOWEST', 1);
define('MOD_MILLIONNAIRE_GRADELATEST', 2);
define('MOD_MILLIONNAIRE_GRADEAVERAGE', 3);
define('MOD_MILLIONNAIRE_GRADENONE', 4);




////////////////////////////////////////////////////////////////////////////////
// Moodle core API                                                            //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the information on whether the module supports a feature
 *
 * @see plugin_supports() in lib/moodlelib.php
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function millionnaire_supports($feature) {
    switch($feature) {
        case FEATURE_MOD_INTRO:                 return true;
        case FEATURE_SHOW_DESCRIPTION:          return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:   return true;
        case FEATURE_GRADE_HAS_GRADE:           return true;
        case FEATURE_GRADE_OUTCOMES:            return true;
        case FEATURE_BACKUP_MOODLE2:            return true;
        default:                                return null;
    }
}

/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the millionnaire.
 *
 * @param $mform form passed by reference
 */
function millionnaire_reset_course_form_definition(&$mform) {
    $mform->addElement('header', MOD_MILLIONNAIRE_MODNAME . 'header', get_string('modulenameplural', MOD_MILLIONNAIRE_LANG));
    $mform->addElement('advcheckbox', 'reset_' . MOD_MILLIONNAIRE_MODNAME , get_string('deletealluserdata',MOD_MILLIONNAIRE_LANG));
}
/**
 * Course reset form defaults.
 * @param object $course
 * @return array
 */
function millionnaire_reset_course_form_defaults($course) {
    return array('reset_' . MOD_MILLIONNAIRE_MODNAME =>1);
}
/**
 * Removes all grades from gradebook
 *
 * @global stdClass
 * @global object
 * @param int $courseid
 * @param string optional type
 */
function millionnaire_reset_gradebook($courseid, $type='') {
    global $CFG, $DB;
    $sql = "SELECT l.*, cm.idnumber as cmidnumber, l.course as courseid
              FROM {" . MOD_MILLIONNAIRE_TABLE . "} l, {course_modules} cm, {modules} m
             WHERE m.name='" . MOD_MILLIONNAIRE_MODNAME . "' AND m.id=cm.module AND cm.instance=l.id AND l.course=:course";
    $params = array ("course" => $courseid);
    if ($moduleinstances = $DB->get_records_sql($sql,$params)) {
        foreach ($moduleinstances as $moduleinstance) {
            millionnaire_grade_item_update($moduleinstance, 'reset');
        }
    }
}
/**
 * Actual implementation of the reset course functionality, delete all the
 * millionnaire attempts for course $data->courseid.
 *
 * @global stdClass
 * @global object
 * @param object $data the data submitted from the reset course.
 * @return array status array
 */
function millionnaire_reset_userdata($data) {
    global $CFG, $DB;
    $componentstr = get_string('modulenameplural', MOD_MILLIONNAIRE_LANG);
    $status = array();
    //if (!empty($data->{'reset_' . MOD_MILLIONNAIRE_MODNAME})) {
        $params = array ("course" => $data->courseid);
        //$DB->delete_records(MOD_MILLIONNAIRE_USERTABLE, $params);
        // remove all grades from gradebook
        if (empty($data->reset_gradebook_grades)) {
            millionnaire_reset_gradebook($data->courseid);
        }
        $status[] = array('component'=>$componentstr, 'item'=>get_string('deletealluserdata', MOD_MILLIONNAIRE_LANG), 'error'=>false);
    //}
    /// updating dates - shift may be negative too
    if ($data->timeshift) {
        shift_course_mod_dates(MOD_MILLIONNAIRE_MODNAME, array('available', 'deadline'), $data->timeshift, $data->courseid);
        $status[] = array('component'=>$componentstr, 'item'=>get_string('datechanged'), 'error'=>false);
    }
    return $status;
}

/**
 * Saves a new instance of the millionnaire into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $millionnaire An object from the form in mod_form.php
 * @param mod_millionnaire_mod_form $mform
 * @return int The id of the newly inserted millionnaire record
 */
function millionnaire_add_instance(stdClass $millionnaire, mod_millionnaire_mod_form $mform = null) {
    global $DB;

    $millionnaire->timecreated = time();
    $millionnaire->id = $DB->insert_record('millionnaire', $millionnaire);

    $millionnaire2 = $DB->get_record('millionnaire', array('id' => $millionnaire->id));
    $millionnaire2->timemodified = time();


    // we need to use context now, so we need to make sure all needed info is already in db
    $DB->set_field('course_modules', 'instance', $millionnaire->id, array('id'=>$millionnaire->coursemodule));
    $context = context_module::instance($millionnaire->coursemodule);


    $textfieldoptions = array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'trusttext' => false,
        'forcehttps' => false,
        'subdirs' => false,
        'maxbytes' => 0,
        );
    $millionnaire2->intro_editor = array();
    $millionnaire2->intro_editor['text'] = $millionnaire->intro;
    $millionnaire2->intro_editor['format'] = FORMAT_HTML;
    $millionnaire2 = file_postupdate_standard_editor($millionnaire2, 'intro', $textfieldoptions, $context,'mod_millionnaire', 'intro', 1);

    $DB->update_record('millionnaire', $millionnaire2);
    // useful for grade
    millionnaire_grade_item_update($millionnaire2);
    return $millionnaire2->id;
}

/**
 * Updates an instance of the millionnaire in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $millionnaire An object from the form in mod_form.php
 * @param mod_millionnaire_mod_form $mform
 * @return boolean Success/Fail
 */
function millionnaire_update_instance(stdClass $millionnaire, mod_millionnaire_mod_form $mform = null) {
    global $DB, $USER;

    // Build game
    $millionnaire->id = $millionnaire->instance;
    $millionnaire->timemodified = time();

    $cm = get_coursemodule_from_instance('millionnaire', $millionnaire->id, $millionnaire->course, false, MUST_EXIST);
    $context = context_module::instance($cm->id);

    $textfieldoptions = array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'trusttext' => false,
        'forcehttps' => false,
        'subdirs' => false,
        'maxbytes' => 0,
        );
    $millionnaire->intro_editor = array();
    $millionnaire->intro_editor['text'] = $millionnaire->intro;
    $millionnaire->intro_editor['format'] = FORMAT_HTML;
    $millionnaire = file_postupdate_standard_editor($millionnaire, 'intro', $textfieldoptions, $context,'mod_millionnaire', 'intro', 1);
    millionnaire_grade_item_update($millionnaire);

    return $DB->update_record('millionnaire', $millionnaire);
}

/**
 * Removes an instance of the millionnaire from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function millionnaire_delete_instance($id) {
    global $DB;

    if (! $millionnaire = $DB->get_record('millionnaire', array('id' => $id))) {
        return false;
    }

    # Delete any dependent records here #

    $DB->delete_records('millionnaire', array('id' => $millionnaire->id));
    $DB->delete_records('millionnaire_grades', array('millionnaire' => $millionnaire->id));

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return stdClass|null
 */
function millionnaire_user_outline($course, $user, $mod, $millionnaire) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $millionnaire the module instance record
 * @return void, is supposed to echp directly
 */
function millionnaire_user_complete($course, $user, $mod, $millionnaire) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in millionnaire activities and print it out.
 * Return true if there was output, or false is there was none.
 *
 * @return boolean
 */
function millionnaire_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;  //  True if anything was printed, otherwise false
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link millionnaire_print_recent_mod_activity()}.
 *
 * @param array $activities sequentially indexed array of objects with the 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 * @return void adds items into $activities and increases $index
 */
function millionnaire_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@see millionnaire_get_recent_mod_activity()}

 * @return void
 */
function millionnaire_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function millionnaire_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * @example return array('moodle/site:accessallgroups');
 * @return array
 */
function millionnaire_get_extra_capabilities() {
    return array();
}

////////////////////////////////////////////////////////////////////////////////
// Gradebook API                                                              //
////////////////////////////////////////////////////////////////////////////////

/**
 * Is a given scale used by the instance of millionnaire?
 *
 * This function returns if a scale is being used by one millionnaire
 * if it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $millionnaireid ID of an instance of this module
 * @return bool true if the scale is used by the given millionnaire instance
 */
function millionnaire_scale_used($millionnaireid, $scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('millionnaire', array('id' => $millionnaireid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Checks if scale is being used by any instance of millionnaire.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param $scaleid int
 * @return boolean true if the scale is used by any millionnaire instance
 */
function millionnaire_scale_used_anywhere($scaleid) {
    global $DB;

    /** @example */
    if ($scaleid and $DB->record_exists('millionnaire', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}

/**
 * Creates or updates grade item for the give millionnaire instance
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $millionnaire instance object with extra cmidnumber and modname property
 * @param mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return void
 */
function millionnaire_grade_item_update(stdClass $millionnaire, $grades=null) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    $item = array();
    $item['itemname'] = clean_param($millionnaire->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;
    $item['grademax']  = $millionnaire->grade;
    $item['grademin']  = 0;

    if ($grades  === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }
    // weird fix (from mod/quiz)
    if (!isset($millionnaire->id)) $millionnaire->id = $millionnaire->instance;

    return grade_update('mod/millionnaire', $millionnaire->course, 'mod', 'millionnaire', $millionnaire->id, 0, $grades, $item);

}

/**
 * Update millionnaire grades in the gradebook
 *
 * Needed by grade_update_mod_grades() in lib/gradelib.php
 *
 * @param stdClass $millionnaire instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 * @return void
 */
function millionnaire_update_grades(stdClass $millionnaire, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    if ($grades = millionnaire_get_user_grades($millionnaire, $userid)) {
        error_log("millionnaire 1");
        millionnaire_grade_item_update($millionnaire, $grades);
    } else if ($userid) {
        error_log("millionnaire 2");
        $grade = new stdClass();
        $grade->userid   = $userid;
        $grade->rawgrade = null;
        $grade->timemodified = time();
        millionnaire_grade_item_update($millionnaire, $grade);

    } else {
        error_log("millionnaire 3");
        millionnaire_grade_item_update($millionnaire);
    }
}


/**
 * Return grade for given user or all users.
 *
 * @global object
 * @global object
 * @param object $forum
 * @param int $userid optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function millionnaire_get_user_grades($millionnaire, $userid = 0) {
    global $CFG, $DB;

    $params = array($millionnaire->id);
    $usertest = '';
    if ($userid) {
        $params[] = $userid;
        $usertest = 'AND u.id = ?';
    }
    return $DB->get_records_sql("
        SELECT
            u.id,
            u.id AS userid,
            mg.grade AS rawgrade,
            mg.timemodified AS dategraded
        FROM {user} u
        JOIN {millionnaire_grades} mg ON u.id = mg.userid

        WHERE mg.millionnaire = ?
        $usertest
        GROUP BY u.id, mg.grade, mg.timemodified", $params);
}

////////////////////////////////////////////////////////////////////////////////
// File API                                                                   //
////////////////////////////////////////////////////////////////////////////////

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function millionnaire_get_file_areas($course, $cm, $context) {
    return array();
}

/**
 * File browsing support for millionnaire file areas
 *
 * @package mod_millionnaire
 * @category files
 *
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function millionnaire_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    return null;
}

////////////////////////////////////////////////////////////////////////////////
// Navigation API                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Extends the global navigation tree by adding millionnaire nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the millionnaire module instance
 * @param stdClass $course
 * @param stdClass $module
 * @param cm_info $cm
 */
function millionnaire_extend_navigation(navigation_node $navref, stdclass $course, stdclass $module, cm_info $cm) {
}

/**
 * Extends the settings navigation with the millionnaire settings
 *
 * This function is called when the context for the page is a millionnaire module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav {@link settings_navigation}
 * @param navigation_node $millionnairenode {@link navigation_node}
 */
function millionnaire_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $millionnairenode=null) {
}

/**
 * Obtains the automatic completion state for this millionnaire based on any conditions
 * in millionnaire settings.
 *
 * @global object
 * @global object
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function millionnaire_get_completion_state($course,$cm,$userid,$type) {
    global $CFG,$DB;

    // Get forum details
    if (!($millionnaire=$DB->get_record('millionnaire',array('id'=>$cm->instance)))) {
        throw new Exception("Can't find millionnaire {$cm->instance}");
    }

    $result=$type; // Default return value

    $postcountparams=array('userid'=>$userid,'millionnaireid'=>$millionnaire->id);
    $postcountsql="
SELECT
    COUNT(1)
FROM
    {forum_posts} fp
    INNER JOIN {forum_discussions} fd ON fp.discussion=fd.id
WHERE
    fp.userid=:userid AND fd.forum=:forumid";

    if ($forum->completiondiscussions) {
        $value = $forum->completiondiscussions <=
                 $DB->count_records('forum_discussions',array('forum'=>$forum->id,'userid'=>$userid));
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($forum->completionreplies) {
        $value = $forum->completionreplies <=
                 $DB->get_field_sql( $postcountsql.' AND fp.parent<>0',$postcountparams);
        if ($type==COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }
    if ($forum->completionposts) {
        $value = $forum->completionposts <= $DB->get_field_sql($postcountsql,$postcountparams);
        if ($type == COMPLETION_AND) {
            $result = $result && $value;
        } else {
            $result = $result || $value;
        }
    }

    return $result;
}


/**
 * Serves the files from the millionnaire file areas
 *
 * @package mod_millionnaire
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the epikmatching's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 */
function millionnaire_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options=array()) {
    global $DB, $CFG;

    if ($context->contextlevel != CONTEXT_MODULE) {
        send_file_not_found();
    }

    require_login($course, true, $cm);
    $entryid = (int)array_shift($args);
    //send_file_not_found();
    $filecontext = context_module::instance($cm->id);
    $relativepath = implode('/', $args);
    $fullpath = "/$filecontext->id/mod_millionnaire/$filearea/$entryid/$relativepath";
    //$fullpath = "/$filecontext->id/mod_millionnaire/intro/1/bouton.png";
    //error_log($fullpath);
    $fs = get_file_storage();
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    send_stored_file($file, 0, 0, true, $options); // download MUST be forced - security!

}
