<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package    mod_millionnaire
 * @copyright  2016 Pascal Fautrero
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/millionnaire/locallib.php');
require_once($CFG->dirroot . '/mod/millionnaire/lib.php');

$millionnaireid = required_param('millionnaireid', PARAM_INT);
$millionnaire   = $DB->get_record('millionnaire', array('id' => $millionnaireid), '*', MUST_EXIST);
$course         = $DB->get_record('course', array('id' => $millionnaire->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('millionnaire', $millionnaire->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);
require_login($course, false, $cm);
if (is_guest($context, $USER)) {
    throw new moodle_exception('noguestsubscribe', 'mod_millionnaire');
}

$module_type = 'millionnaire';
$module_id = $cm->id;
rebuild_course_cache($course->id, true);
$navigationModuleButton = new millionnaireNavigationModule($course->id, $module_type, $module_id);
$nextModule = $navigationModuleButton->html();

echo json_encode(array(
    'status'   => 'OK',
    'contents' => $nextModule,
));
