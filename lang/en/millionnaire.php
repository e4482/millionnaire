<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for millionnaire
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_millionnaire
 * @copyright  2011 Your Name
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Jeu du millionnaire';
$string['modulenameplural'] = 'Millionnaires';
$string['modulename_help'] = ""
        . "<h2>Jeu du millionnaire</h2>"
        . '<p>Cette activité vous permet de créer un jeu du type "Qui veut gagner des millions ?".</p>'
        . "";
$string['millionnairefieldset'] = 'Custom example fieldset';
$string['millionnairename'] = 'millionnaire';
$string['millionnairename_help'] = 'Jeu type "Qui veut gagner des millions ?"';
$string['millionnaire'] = 'millionnaire';
$string['pluginadministration'] = 'millionnaire administration';
$string['pluginname'] = 'millionnaire';
$string['millionnaire:addinstance'] = 'ajouter un module millionnaire';
$string['deletealluserdata'] = 'Supprimer toutes les tentatives des millionnaires';
$string['grade'] = 'note';
$string['grade_help'] = 'note maximum attribuée';
