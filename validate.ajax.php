<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Ajax script to update the contents of the question bank dialogue.
 *
 * @package    mod_millionnaire
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


// defined is used here to make unit test possibles
// weird hack...
if (!defined('AJAX_SCRIPT')) define('AJAX_SCRIPT', true);
global $DB, $USER;

require_once(__DIR__ . '/../../config.php');
require_once($CFG->dirroot . '/mod/millionnaire/locallib.php');
require_once($CFG->dirroot . '/mod/millionnaire/lib.php');

$millionnaireid = required_param('mid', PARAM_INT);             // The forum to subscribe or unsubscribe.
$attempts = required_param('attempts', PARAM_INT);
//$sesskey        = required_param('sesskey', PARAM_INT);
$millionnaire   = $DB->get_record('millionnaire', array('id' => $millionnaireid), '*', MUST_EXIST);
$course         = $DB->get_record('course', array('id' => $millionnaire->course), '*', MUST_EXIST);
$cm             = get_coursemodule_from_instance('millionnaire', $millionnaire->id, $course->id, false, MUST_EXIST);
$context        = context_module::instance($cm->id);

//require_sesskey($sesskey);
require_login($course, false, $cm);
//require_capability('mod/forum:viewdiscussion', $context);

$return = new stdClass();

if (is_guest($context, $USER)) {
    // is_guest should be used here as this also checks whether the user is a guest in the current course.
    // Guests and visitors cannot subscribe - only enrolled users.
    throw new moodle_exception('noguestsubscribe', 'mod_millionnaire');
}


error_log("manage completion");
// Manage completion
$completion = new completion_info($course);
$completion->set_module_viewed($cm);
$completion->update_state($cm,COMPLETION_COMPLETE);

error_log("manage gradeBook");
// Manage gradeBook
$millionnaireGrade = $DB->get_record('millionnaire_grades', array('userid' => $USER->id, 'millionnaire' => $millionnaire->id));
if ($millionnaireGrade) {
    $millionnaireGrade->grade = 30;
    $DB->update_record('millionnaire_grades', $millionnaireGrade);
}
else {
    $millionnaireGrade = new stdClass();
    $millionnaireGrade->millionnaire = $millionnaire->id;
    $millionnaireGrade->userid = $USER->id;
    $millionnaireGrade->grade = 30;
    $millionnaireGrade->timemodified = time();
    $millionnaireGrade->attempts = $attempts;
    $DB->insert_record("millionnaire_grades", $millionnaireGrade);
}
millionnaire_update_grades($millionnaire, $USER->id);


$contents = "C'est mon dernier mot Jean-Pierre";
echo json_encode(array(
    'status'   => 'OK',
    'contents' => $contents,
));
