<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Internal library of functions for module millionnaire
 *
 * All the millionnaire specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_millionnaire
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


class millionnaireNavigationModule {

  protected $courseid;
  protected $current_module_type;
  protected $current_module_id;
  protected $current_course_module;
  protected $section_parent;

  /***************************************************
   *
   * return void
   *
   * *************************************************/
  public function __construct($courseid, $moduletype, $moduleid) {

    $this->courseid = $courseid;
    $this->current_module_type = $moduletype;
    $this->current_module_id = $moduleid;

  }
  /***************************************************
   *
   * return @array
   *
   * *************************************************/
  public function get_modules_list() {
    global $DB;
    $course_sections = $DB->get_records_sql("
        SELECT id, sequence
        FROM {course_sections}
        WHERE course = '" . $this->courseid . "'
        ORDER BY section ASC");
    $global_sequence = "";
    $final_array = [];
    foreach($course_sections as $course_section) {
        $global_sequence .= $course_section->sequence . ",";
    }

    $course = $DB->get_record('course',array('id' => $this->courseid));
    $modinfo = get_fast_modinfo($course);
    $modules_array = explode(",", $global_sequence);
    foreach ($modules_array as $cmid) {
      if ($cmid) {
        $cm = $modinfo->get_cm($cmid);
        if (($cm->visible) && ($cm->uservisible)) {
          array_push($final_array, $cmid);
        }
      }
    }
    // get the next module
    $this->current_course_module = $DB->get_record_sql("
        SELECT cm.id
        FROM {course_modules} as cm, {modules} as m
        WHERE cm.course = '" . $this->courseid . "'
        AND cm.module = m.id
        AND m.name = '" . $this->current_module_type . "'
        AND cm.instance = " . $this->current_module_id . "
    ");

    return $final_array;

  }
  public function get_parent_section($course_module_id) {

    global $DB;
    $parent_section = null;
    $course_sections = $DB->get_records_sql("
      SELECT id, sequence, section
      FROM {course_sections}
      WHERE course = '" . $this->courseid . "'
      ORDER BY section ASC");
    $global_sequence = "";
    foreach($course_sections as $course_section) {
      $sequence_array = explode(",", $course_section->sequence);
      if (in_array($course_module_id, $sequence_array)) {
        $parent_section = $course_section->section;
      }
    }
    return $parent_section;

  }
  /****************************************************
   *
   *
   *
   * **************************************************/

  public function html() {

    global $DB;

    $global_sequence_array = $this->get_modules_list();
    $nextModule = "";
    $key = array_search($this->current_module_id, $global_sequence_array);

    if ($key !== false) {
      if (array_key_exists($key + 1, $global_sequence_array) &&
        $global_sequence_array[$key + 1] != '') {
        $next_course_module = $DB->get_record_sql("
          SELECT cm.instance, m.name
          FROM {course_modules} as cm, {modules} as m
          WHERE cm.id = " . $global_sequence_array[$key + 1] . "
          AND m.id = cm.module
        ");
        if ($next_course_module->name != 'label') {
          $nextModule = "<a href='/mod/"
            . $next_course_module->name
            . "/view.php?id="
            . $global_sequence_array[$key + 1]
            . "'><img src='/mod/millionnaire/pix/next.png' title='étape suivante' alt='next' style='margin:5px;' /></a>";
        }
        else {
          $nextModule = "<a href='/course/"
            . "view.php?id="
            . $this->courseid
            . "&section="
            . $this->get_parent_section($global_sequence_array[$key + 1])
            . "'><img src='/mod/millionnaire/pix/next.png' title='étape suivante' style='margin:5px;' alt='next' /></a>";
        }
      }

    }
    return $nextModule;
  }
}

function getActiveFilters() {

    global $DB;
    $recs = $DB->get_records_sql("SELECT * FROM {filter_active} WHERE active = '1' ORDER BY sortorder ASC ");
    return $recs;
}

function applyFilters($content, $active_filters, $context, $filearea)
{

    global $CFG;
    $content = file_rewrite_pluginfile_urls($content, 'pluginfile.php', $context->id, 'mod_millionnaire', $filearea, null);
    foreach ($active_filters as $currentfilter) {
        if (file_exists($CFG->dirroot . '/filter/' . $currentfilter->filter . '/filter.php')) {
            require_once($CFG->dirroot . '/filter/' . $currentfilter->filter . '/filter.php');
            $class_filter = "filter_" . $currentfilter->filter;
            $filterplugin = new $class_filter($context, array());
            $original_content = $content;
            $content = $filterplugin->filter($original_content, array('originalformat' => '0'));
        }
    }

    return $content;
}



/**
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 */
//function millionnaire_do_something_useful(array $things) {
//    return new stdClass();
//}

function millionnaire_create_game(stdClass $millionnaire, $map){
    global $USER, $COURSE, $CFG;

    $bg_img = new moodle_url('/mod/millionnaire/pix/back.jpg');
    $gameid = "game" . rand(100,999);
    $gameid_lost = $gameid . "_lost";
    $gameid_reload = $gameid . "_reload";
    $gameid_won = $gameid . "_won";
    $gameid_points = $gameid . "_points";
    $gameid_first_step = $gameid . "_first_step";
    $active_filters = getActiveFilters();
    $desc = "";
    $title = $millionnaire->name;
    $cm = get_coursemodule_from_instance('millionnaire', $millionnaire->id, $millionnaire->course, false, MUST_EXIST);
    $context = context_module::instance($cm->id);

    if ($millionnaire->intro != "") {
      $desc = applyFilters($millionnaire->intro, $active_filters, $context, "intro");
      $hideheader = "";
    }
    else {
      $hideheader = "hideheader";
    }

    $questions = array();
    if ($millionnaire->question_1 == '') $millionnaire->question_1 = "42 ?";
    for ($i = 1;$i <=6; $i++){
        $question = 'question_' . $i;
        $goodanswer = 'goodanswer_' . $i;
        $badanswer1 = 'badanswer1_' . $i;
        $badanswer2 = 'badanswer2_' . $i;
        $badanswer3 = 'badanswer3_' . $i;

        if ($millionnaire->$question != '') {
            $questions[] = array(
                'text' => $millionnaire->$question,
                'weight' => '500',
                'answers' => array(
                    array(
                        'text' => $millionnaire->$goodanswer,
                        'type' => 'good'),
                    array(
                        'text' => $millionnaire->$badanswer1,
                        'type' => 'bad'),
                    array(
                        'text' => $millionnaire->$badanswer2,
                        'type' => 'bad'),
                    array(
                        'text' => $millionnaire->$badanswer3,
                        'type' => 'bad')
                )
            );
        }
    }

    $questions_json = json_encode($questions, JSON_PRETTY_PRINT);

    $module_type = 'millionnaire';
    $module_id = $cm->id;
    $navigationModuleButton = new millionnaireNavigationModule($COURSE->id, $module_type, $module_id);
    $nextModule = $navigationModuleButton->html();

    $maplogo = "";
    if ($map) {
        $maplogo = "<a href='/mod/mapmodules/view.php?id=$map'><img src='/mod/millionnaire/pix/map.png' alt='Retourner à la map' title='Retourner à la map'></a>";
    }

  ob_start();
  require($CFG->dirroot . '/mod/millionnaire/template/index.html');
  $game = ob_get_contents();
  ob_end_clean();
  return $game;

}
