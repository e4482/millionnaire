<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_data
 * @subpackage backup-moodle2
 * @copyright 2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the backup steps that will be used by the backup_data_activity_task
 */

/**
 * Define the complete data structure for backup, with file and id annotations
 */
class backup_millionnaire_activity_structure_step extends backup_activity_structure_step {

    protected function define_structure() {

        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $millionnaire = new backup_nested_element('millionnaire', array('id'), array(
            'course', 'name', 'intro', 'introformat', 'timecreated', 'timemodified', 
            'question_1', 'goodanswer_1', 'badanswer1_1', 'badanswer2_1', 'badanswer3_1', 
            'question_2', 'goodanswer_2', 'badanswer1_2', 'badanswer2_2', 'badanswer3_2', 
            'question_3', 'goodanswer_3', 'badanswer1_3', 'badanswer2_3', 'badanswer3_3', 
            'question_4', 'goodanswer_4', 'badanswer1_4', 'badanswer2_4', 'badanswer3_4', 
            'question_5', 'goodanswer_5', 'badanswer1_5', 'badanswer2_5', 'badanswer3_5', 
            'question_6', 'goodanswer_6', 'badanswer1_6', 'badanswer2_6', 'badanswer3_6', 
            ));
      
        $millionnaire->set_source_table('millionnaire', array('id' => backup::VAR_ACTIVITYID));

        $millionnaire->annotate_files('mod_millionnaire', 'intro', null); // This file area hasn't itemid
        // Return the root element (data), wrapped into standard activity structure
        return $this->prepare_activity_structure($millionnaire);
    }
}
