<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of millionnaire
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_millionnaire
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/// (Replace millionnaire with the name of your module and remove this line)

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/locallib.php');

$id = optional_param('id', 0, PARAM_INT); // course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // millionnaire instance ID - it should be named as the first character of the module

$map = 0;
$map = optional_param('map', 0, PARAM_INT);



if ($id) {
    $cm         = get_coursemodule_from_id('millionnaire', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $millionnaire  = $DB->get_record('millionnaire', array('id' => $cm->instance), '*', MUST_EXIST);
} elseif ($n) {
    $millionnaire  = $DB->get_record('millionnaire', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $millionnaire->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('millionnaire', $millionnaire->id, $course->id, false, MUST_EXIST);
} else {
    print_error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);
$context = context_module::instance($cm->id);

$PAGE->set_url('/mod/millionnaire/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($millionnaire->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);

echo $OUTPUT->header();
echo millionnaire_create_game($millionnaire, $map);
echo $OUTPUT->footer();
