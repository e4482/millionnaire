<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main millionnaire configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_millionnaire
 * @copyright  2015 Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 */
class mod_millionnaire_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {

        global $CFG;
        $mform = $this->_form;

        //-------------------------------------------------------------------------------
        // Adding the "general" fieldset, where all the common settings are showed
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field
        $mform->addElement('text', 'name', "Titre : ", array('size'=>'64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEAN);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'millionnairename', 'millionnaire');


        // Adding the standard "intro" and "introformat" fields
        //$this->add_intro_editor();

        // tricky trick !!
        //$mform->addElement('html', '<div style="display:none;">');
        //$mform->addElement('editor', 'introeditor', "Description", array('rows' => 10), array('maxfiles' => EDITOR_UNLIMITED_FILES,
        //    'noclean' => true, 'context' => $this->context, 'subdirs' => true));
        //$mform->setType('introeditor', PARAM_RAW); // no XSS prevention here, users must be trusted
        //$mform->addElement('html', '</div>');

        $this->standard_intro_elements();


        //$checkbox = $mform->addElement('checkbox', 'showdescription', get_string('showdescription'));

        //YUI.one('showdescription').set('checked', true);

        //$mform->addHelpButton('showdescription', 'showdescription');

        $mform->addElement('text', "grade", "nombre de points", array('size'=>'5'));
        $mform->setType("grade", PARAM_TEXT);
        $mform->setDefault('grade', 10);
        $mform->addHelpButton('grade', 'grade', 'millionnaire');

        //-------------------------------------------------------------------------------
        // Adding the rest of millionnaire settings, spreeading all them into this fieldset
        // or adding more fieldsets ('header' elements) if needed for better logic
        //$mform->addElement('static', 'label1', 'millionnairesetting1', 'Your millionnaire fields go here. Replace me!');

        // 500 - 1000 - 10000 - 50000 - 250000 - 1000000


        $mform->addElement('html', '<h4>Question très facile (500)</h4>');
        $this->addTextField($mform, "question_1", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_1", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_1", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_1", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_1", "Mauvaise réponse");

        $mform->addElement('html', '<h4>Question facile (1000)</h4>');
        $this->addTextField($mform, "question_2", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_2", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_2", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_2", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_2", "Mauvaise réponse");

        $mform->addElement('html', '<h4>Question peu difficile (10000)</h4>');
        $this->addTextField($mform, "question_3", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_3", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_3", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_3", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_3", "Mauvaise réponse");

        $mform->addElement('html', '<h4>Question assez difficile (50000)</h4>');
        $this->addTextField($mform, "question_4", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_4", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_4", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_4", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_4", "Mauvaise réponse");

        $mform->addElement('html', '<h4>Question difficile (250000)</h4>');
        $this->addTextField($mform, "question_5", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_5", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_5", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_5", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_5", "Mauvaise réponse");

        $mform->addElement('html', '<h4>Question très difficile (1000000)</h4>');
        $this->addTextField($mform, "question_6", "Question",'background-color:#dddddd;');
        $this->addTextField($mform, "goodanswer_6", "Bonne réponse");
        $this->addTextField($mform, "badanswer1_6", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer2_6", "Mauvaise réponse");
        $this->addTextField($mform, "badanswer3_6", "Mauvaise réponse");
        //-------------------------------------------------------------------------------
        // add standard elements, common to all modules
        $this->standard_coursemodule_elements();
        //-------------------------------------------------------------------------------
        // add standard buttons, common to all modules
        $this->add_action_buttons();
    }
    /**
     * Defines forms elements
     */
    public function addTextField($mform, $id, $label, $style='') {
        $mform->addElement('text', $id, $label, array('size'=>'64', 'style'=>$style));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType($id, PARAM_TEXT);
        } else {
            $mform->setType($id, PARAM_CLEAN);
        }
    }
    function definition_after_data(){
        //var_dump(($this->_form->_defaultValues["intro"]));

        // mandatory for completion !
        parent::definition_after_data();

    }
}
